package com.rynok_company.vkclient.vkclient.app;


import android.util.Log;

import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.rynok_company.vkclient.vkclient.app.api.VKApi;

public class LoginActivity extends Activity implements OnClickListener {
	private static final String[] scope = new String[] {
			VKScope.FRIENDS,
			VKScope.WALL,
			VKScope.PHOTOS,
			VKScope.NOHTTPS
	};

	public static enum TransitionType {  
        Zoom, SlideLeft, Diagonal  
	}
	
	public static TransitionType transitionType;
	account account=new account();
    VKApi api;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.login);
				
		Button loginBtn = (Button)findViewById(R.id.loginBtn);  
		loginBtn.setOnClickListener(this); 	
		
		FrameLayout frameReg = (FrameLayout)findViewById(R.id.frameReg);
		frameReg.setOnClickListener(this);
				
		helper.WriteInfo("� ������� �� Api level=" + Build.VERSION.SDK_INT);
		
		if (Build.VERSION.SDK_INT < 8) {
			helper.WriteInfo("���� ����� ���� ���");
		} else {
			helper.WriteInfo("��� ����� ����� ���");
		}
		
		account.restore(this);
        
        if(account.access_token!=null) {
            api=new VKApi(account.access_token);
            
            finish();
            
            Intent intent = new Intent(this, MainActivity.class);  
            startActivity(intent);
        }		
    }
    
    public void onClick(View v) {        
        switch(v.getId()) {
        case R.id.loginBtn:        	
        	EditText phoneNumber   = (EditText)findViewById(R.id.etPhoneNumber);
        	EditText password   = (EditText)findViewById(R.id.etPassword);
        	
        	String login = phoneNumber.getText().toString();
        	String passwd = password.getText().toString();
        	        	
        	if (helper.isNullOrEmpty(login) && helper.isNullOrEmpty(passwd)) {
        		Toast.makeText(this, "������� ����� �������� � ������", Toast.LENGTH_SHORT).show();
			} else if (helper.isNullOrEmpty(login)) {
				Toast.makeText(this, "������� ����� ��������", Toast.LENGTH_SHORT).show();
			} else if (helper.isNullOrEmpty(passwd)) {
				Toast.makeText(this, "������� ������", Toast.LENGTH_SHORT).show();
			} else {   		
        		String result = "";
				result =VKApi.Auth(login, passwd);
        		            		            		
        		if (result.equals("401")) {
        			Toast.makeText(this, "401: �������� ����� ��� ������", Toast.LENGTH_SHORT).show();	
				} else if (result.equals("404")) {
					Toast.makeText(this, "404: ����� �������", Toast.LENGTH_SHORT).show();
				} else if (result.equals("Socket is not connected")) {
					Toast.makeText(this, "��� ����������", Toast.LENGTH_SHORT).show();
				}
        		else {						

						//JSONObject jToken = new JSONObject(result);

						String access_token = "4be312de152d370de3d86d5ec9475d171fe8198bdaff8f723db00ea4be274255aadbef0105407bbeb9740";
						long user_id = 57068981;
						
						account.access_token= access_token;
		                account.user_id=user_id;
								                
						account.save(LoginActivity.this);		
						
						if (access_token != null) {
							finish();

				            Intent intent = new Intent(this, MainActivity.class);  
				            startActivity(intent);
						}
						

        		}
        	}
            break;
        case R.id.frameReg:
        	Intent intent = new Intent(this, RegistrationActivity.class);  
            startActivity(intent);
        	
        	transitionType = TransitionType.SlideLeft;  
            overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
        	
        	break;
        }
    }
}